"use strict";

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const Jarvis = require("webpack-jarvis");
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: './src/index.jsx',
    output: {
        path: path.resolve(__dirname, "dist"),
        publicPath: "/",
        filename: 'app.bundle.js'
    },
    devServer: {
        inline: true,
        port: 3131,
        host: 'localhost',
        disableHostCheck: true,

    },
    watchOptions: {
        poll: true
    },
    stats: {
        assets: true,
        colors: true,
        errors: true,
        errorDetails: true,
        hash: true,
    },
    devtool: 'inline-source-map',
    mode: 'development',
    module: {
        rules: [{
            test: /.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader",
                // publicPath: "/assets/"
            })
        },
        {
            test: /\.(less)$/,
            use: [{
                loader: 'style-loader' // creates style nodes from JS strings
            }, {
                loader: 'css-loader' // translates CSS into CommonJS
            }, {
                loader: 'less-loader' // compiles Less to CSS
            }]
        },
        {
            test: /\.(png|gif|jpg|jpeg|)$/,
            loader: "file-loader",
            options: {
                limit: 100000,
                name: '[name].[ext]',
                outputPath: 'dist/images'
            }
        },
        {
            test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                limit: 10000,
                mimetype: 'application/font-woff',
                name: '[name].[ext]',
                outputPath: 'dist/fonts'
            }
        },
        {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                limit: 10000,
                mimetype: 'application/octet-stream',
                name: '[name].[ext]',
                outputPath: 'dist/fonts'
            }
        },
        {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'dist/fonts'
            }
        },
        {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                limit: 10000,
                mimetype: 'image/svg+xml',
                name: '[name].[ext]',
                outputPath: 'dist/svg'
            }
        },
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
        },
        {
            test: /\.styl$/,
            loader: "style-loader!css-loader!stylus-loader"
        }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("development"),
                APPLICATION_BFF_URL: JSON.stringify(""),
            }
        }),
        new ExtractTextPlugin({
            filename: "bundle.css",
            disable: false,
            allChunks: true
        }),
        new Jarvis({
            port: 1337
        }),
        new HtmlWebpackPlugin({
            template: 'index.html'
        })

    ]
};

