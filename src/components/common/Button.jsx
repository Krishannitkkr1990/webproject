import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';

function GenericButton({
  btnType, btnText, btnClassName, insertSpaceAfterIcon, children, onClickHandler, disabled = false,
}) {
  return (
    <Button type={btnType} className={btnClassName} onClick={onClickHandler} disabled={disabled}>
      {children}{insertSpaceAfterIcon ? ' ' : ''}
      {btnText}
    </Button>
  );
}


export function AddNewButton({
  disabled = false, btnText, btnClassName, insertSpaceAfterIcon, btnSpace = '', children, onClickHandler,
}) {
  return (
    <GenericButton disabled={disabled} btnText={btnText} insertSpaceAfterIcon={insertSpaceAfterIcon} disabled={disabled} btnType="button" btnClassName={btnClassName || 'btn btn-info'} onClickHandler ={onClickHandler || function () { alert('no action on click'); }}>
      {children || <span className="glyphicon glyphicon-plus"></span>}
    </GenericButton>
  );
}







