
// PARTY CONSTANTS

export const REQUEST_PARTY_DETAILS_DATA = 'REQUEST_PARTY_DETAILS_DATA';
export const RECEIVED_PARTY_DETAILS_DATA = 'RECEIVED_PARTY_DETAILS_DATA';

// CURRENT BALANCE CONSTANTS

export const REQUEST_CURRENT_BALANCE_DATA = 'REQUEST_CURRENT_BALANCE_DATA';
export const RECEIVED_CURRENT_BALANCE_DATA = 'RECEIVED_CURRENT_BALANCE_DATA'