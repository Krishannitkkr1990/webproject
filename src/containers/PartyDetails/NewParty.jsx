import React from 'react';
import Redirect from 'react-router/Redirect';
import connect from 'react-redux/lib/connect/connect';
import PersonIcon from 'material-ui-icons/Person';
import IconButton from 'material-ui/IconButton';
import AutoComplete from '../../components/common/AutoComplete.jsx';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';


class NewParty extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            partyDetailsDataLookup: [],
            partyName: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        if (!_isEmpty(nextProps.partyDetailsData) && !_isEmpty(nextProps.currentBalanceData)) {
            const partyDetailsDataLookup = nextProps.partyDetailsData.map(obj => {
                return {
                    value: obj.id,
                    displayText: `${_get(obj, 'name')} ${_get(obj, 'address')} , ${_get(obj, 'city')}, ${_get(obj, 'state')}, ${_get(obj, 'pincode')}, ${_get(obj, 'country')}`
                }
            });

            this.setState({ partyDetailsDataLookup: partyDetailsDataLookup })
        }

    }

    handleSelectChange = (name, value) => {
        this.setState({ [name]: value });
    }

    render() {

        return (<div className="row">
            <div className="col-sm-6 col-xs-8">
                <ul className="breadcrumb">
                    <li>Party Details</li>
                </ul>
            </div>
            <div className="row">
                <IconButton ariaLabel='personIcon'>
                    <PersonIcon />
                </IconButton>
                <AutoComplete
                    type="single"
                    data={_get(this.state, 'partyDetailsDataLookup', [])}
                    placeholder="Enter Party Name"
                    name="partyName"
                    value={_get(this.state, 'partyName', '')}
                    changeHandler={id => this.handleSelectChange('partyName', id)}
                />
            </div>
        </div >);
    }
}



const mapStateToProps = (state) => {
    const { partyReducer } = state;

    const { partyDetailsData, currentBalanceData } = partyReducer || [];

    return {
        partyDetailsData,
        currentBalanceData
    };
};

export default connect(mapStateToProps)(NewParty);
