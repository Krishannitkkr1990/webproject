import React from 'react';
import Redirect from 'react-router/Redirect';
import connect from 'react-redux/lib/connect/connect';
import PersonIcon from 'material-ui-icons/Person';
import IconButton from 'material-ui/IconButton';
import { AddNewButton } from '../../components/common/Button.jsx';
import {
    fetchPartyDetailsData,
    fetchCurrentBalanceData
} from '../../actions/partyAction';



class PartyDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToNewPartyPage: false
        };
    }

    componentDidMount() {

    }

    handleAddPartyClick = () => {
        this.setState({redirectToNewPartyPage: true});
        const partyDetailsUrl = "http://admin-bff.getsandbox.com/partyDetailsData";
        const currentBalanceUrl = "http://admin-bff.getsandbox.com/currentBalanceData";

        this.props.getPartyData(partyDetailsUrl);
        this.props.getCurrentBalanceData(currentBalanceUrl);

    }

    render() {

        if (this.state.redirectToNewPartyPage) {
            return <Redirect push to="/newparty" />;
        }


        return (<div className="row">
            <div className="col-sm-6 col-xs-8">
                <ul className="breadcrumb">
                    <li>Party Details</li>
                </ul>
            </div>
            <div className="col-sm-6 col-xs-4" >
                <span>
                    <IconButton ariaLabel='personIcon'>
                        <PersonIcon />
                    </IconButton>
                    <AddNewButton
                        btnText='add Party'
                        onClickHandler={this.handleAddPartyClick}
                        insertSpaceAfterIcon={true}
                    />
                </span>

            </div>
        </div >);
    }
}

const mapDispatchToProps = dispatch => ({
    getPartyData: (url) => dispatch(fetchPartyDetailsData(url)),
    getCurrentBalanceData: (url) =>  dispatch(fetchCurrentBalanceData(url))
});

const mapStateToProps = (state) => {
    const { partyDetailsReducer } = state;

    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PartyDetails);
