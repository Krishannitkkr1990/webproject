// system file import

import React from 'react';
import thunk from 'redux-thunk';
import ReactDom from 'react-dom';
import { Switch } from 'react-router-dom';
import Provider from 'react-redux/lib/components/Provider';
import { createLogger } from 'redux-logger/src/index';
import createStore from 'redux/lib/createStore';
import applyMiddleware from 'redux/lib/applyMiddleware';
import Router from 'react-router-dom/HashRouter';
import Route from 'react-router-dom/Route';

// user file import

import reducer from './reducers';
import PartyDetails from './containers/PartyDetails/PartyDetails.jsx';
import NewParty from './containers/PartyDetails/NewParty.jsx';

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}


export const store = createStore(
  reducer,
  applyMiddleware(...middleware)
);


ReactDom.render(
  <div>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" component={PartyDetails} />
          <Route exact path="/newparty" component={NewParty} />
        </Switch>
      </Router>
    </Provider>
  </div>,
  document.getElementById('content-wrapper'),
);
