import {
    REQUEST_PARTY_DETAILS_DATA,
    RECEIVED_PARTY_DETAILS_DATA,
    REQUEST_CURRENT_BALANCE_DATA,
    RECEIVED_CURRENT_BALANCE_DATA
} from '../constants/partyConstants';


const partyReducer = (state = {
    isFetching: false,
    type: '',
    partyDetailsData: [],
    currentBalanceData: []
}, action) => {
    switch (action.type) {

        case REQUEST_PARTY_DETAILS_DATA:
            return Object.assign({}, state, {
                isFetching: true,
                type: action.type
            });
        case RECEIVED_PARTY_DETAILS_DATA:
            return Object.assign({}, state, {
                isFetching: false,
                type: action.type,
                partyDetailsData: action.data
            });
        case REQUEST_CURRENT_BALANCE_DATA:
            return Object.assign({}, state, {
                isFetching: true,
                type: action.type
            });
        case RECEIVED_CURRENT_BALANCE_DATA:
            return Object.assign({}, state, {
                isFetching: false,
                type: action.type,
                currentBalanceData: action.data
            });

        default:
            return state;
    }
};


export default partyReducer;
