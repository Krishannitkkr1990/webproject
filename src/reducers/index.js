
import combineReducers from 'redux/lib/combineReducers';
import partyReducer from './partyReducer';

const initialReducer = (state = {
    isFetching: false,
    type: '',
}, action) => {
    switch (action.type) {
        default:
            return state;
    }
};


const rootReducer = combineReducers({
    initialReducer,
    partyReducer
});

export default rootReducer;
