import {
    REQUEST_PARTY_DETAILS_DATA,
    RECEIVED_PARTY_DETAILS_DATA,
    REQUEST_CURRENT_BALANCE_DATA,
    RECEIVED_CURRENT_BALANCE_DATA
} from '../constants/partyConstants';


/*  action start*/


export const requestPartyDetailsData = () => ({
    type: REQUEST_PARTY_DETAILS_DATA
});

export const receivePartyDetailsData = (json) => ({
    type: RECEIVED_PARTY_DETAILS_DATA,
    data: json,
    receivedAt: Date.now(),
});

export const fetchPartyDetailsData = (url) => (dispatch) => {
    dispatch(requestPartyDetailsData());
    return fetch(url)
        .then(response => response.json())
        .then(json => dispatch(receivePartyDetailsData(json)));
};


/* action end */


/*  action start*/


export const requestCurrentBalanceData = () => ({
    type: REQUEST_CURRENT_BALANCE_DATA
});

export const receiveCurrentBalanceData = (json) => ({
    type: RECEIVED_CURRENT_BALANCE_DATA,
    data: json,
    receivedAt: Date.now(),
});

export const fetchCurrentBalanceData = (url) => (dispatch) => {
    dispatch(requestCurrentBalanceData());
    return fetch(url)
        .then(response => response.json())
        .then(json => dispatch(receiveCurrentBalanceData(json)));
};



      /* action end */
